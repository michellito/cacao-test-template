variable "username" {
  type = string
  description = "username"
}

variable "project" {
  type = string
  description = "project name"
}

variable "region" {
  type = string
  description = "string, openstack region name; default = IU"
  default = "IU"
}

variable "instance_name" {
  type = string
  description = "name of instance"
}

variable "image_uuid" {
  type = string
  description = "string, image id; image will have priority if both image and image name are provided"
  default = ""
}

variable "image" {
  type = string
  description = "string, image id; image will have priority if both image and image name are provided"
  default = ""
}

variable "image_name" {
  type = string
  description = "string, name of image; image will have priority if both image and image name are provided"
  default = "Featured-Ubuntu22"
}

variable "flavor" {
  type = string
  description = "flavor or size of instance to launch"
  default = "m1.tiny"
}

variable "frosting" {
  type = string
  description = "frosting flavor"
  default = "chocolate"
}

variable "make_gluten_free" {
  type = bool
  description = "whether cake should be gluten free"
  default = false
}

variable "number_of_layers" {
  type = number
  description = "number of layers in the cake"
  default = 2
}

variable "tip" {
  type = number
  description = "dollar amount to tip the baker"
  default = 0
}

variable "base_64_test" {
  type = string
  description = "Test for base64 encoded string"
}

variable "keypair" {
  type = string
  description = "keypair to use when launching"
  default = ""
}

variable "power_state" {
  type = string
  description = "power state of instance"
  default = "active"
}

variable "user_data" {
  type = string
  description = "cloud init script"
  default = ""
}
